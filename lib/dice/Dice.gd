# extends Node
class_name Dice, 'res://lib/dice/dice.jpg'

var _min:int
var _max:int 

func setMinMax(set_min:int, set_max:int):
	_min = set_min
	_max = set_max
#
func roll( need_min:int=0,  need_max:int=0)-> int:
	if(need_min == need_max):
		return randi()%(_max-_min+1)+_min;
	else:
		return randi()%(need_max-need_min+1)+need_min;	
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

#func roll() -> int:
#	return randi()%(_max-_min+1)+_min;

func _init(set_min:int = 1, set_max:int = 6):
	randomize()
	_min = set_min
	_max = set_max


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
