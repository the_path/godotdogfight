class_name DicePool, 'res://lib/dice/dice.jpg'


var _arrayDice = []
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

func addDice(dice):
	_arrayDice.push_back(dice)

func lenght()->int:
	return _arrayDice.size()

func getDice(position)->int:
	return _arrayDice[position]
	
func changeDice(position,need_min,need_max):
	_arrayDice[position].setMinMax(need_min,need_max)

func removeDice(position):
	_arrayDice.remove(position)

func rollDice() -> Array:
	var _arr=[]
	for dice in _arrayDice:
		_arr.push_back(dice.roll())
	return _arr

func rollAndSum()->int:
	var sum=0
	for dice in _arrayDice:
		sum += dice.roll()
	return sum

func _init(dice_array):
	if(dice_array is Array):
		_arrayDice.append_array(dice_array)
		
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
