extends Node
class_name DogFighter

signal die
signal ready_str(v)
signal ready_con(v)
signal ready_dex(v)
signal change_str(v)
signal change_con(v)
signal change_dex(v)
signal change_defence(v)

var _str:int=10
var _con:int=10
var _dex:int=10
var _str_speed:float=0
var _con_speed:float=0
var _dex_speed:float=0
var _str_block:bool=0
var _con_block:bool=0
var _dex_block:bool=0
var _analize:int=10
var _memory:int=10
var _perception:int=10
var isDie:bool = false
var isFight:bool = false
var _dices:DicePool
var _arrDice
var _speed:float = 30
var _defence:float=10
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
func reinit():
	_str = 10
	_con = 10
	_dex = 10
	_str_speed = 0
	_con_speed = 0
	_dex_speed = 0
	_defence = 10
	_str_block = false
	_con_block = false
	_dex_block = false
	emit_signal("change_str",_str)
	emit_signal("change_con",_con)
	emit_signal("change_dex",_dex)
	emit_signal("change_defence",_defence)
	
func doDMG(toStr:int=0,toCon:int=0 ,toDex:int=0):
	if(toStr>0):
		_defence -=toStr
		if(_defence<0):
			_str += _defence
			_defence = 0
			emit_signal("change_str",_str)
		emit_signal("change_defence",_defence)
	if(toCon>0):
		_defence -=toCon
		if(_defence<0):
			_con += _defence
			_defence = 0
			emit_signal("change_con",_con)
		emit_signal("change_defence",_defence)
	if(toDex>0):
		_defence -=toDex
		if(_defence<0):
			_dex += _defence
			_defence = 0
			emit_signal("change_dex",_dex)
		emit_signal("change_defence",_defence)
		
func doAttDex()->int:
	_dex_block = false
	return (_arrDice[2]+_dex)/3
func doDefDex()->int:
	_dex_block = false
	_defence += (_arrDice[2]+_dex)/3
	emit_signal("change_defence",_defence)
	return (_arrDice[2]+_dex)/3
func doAttCon()->int:
	_con_block = false
	return (_arrDice[1]+_con)/3
func doDefCon()->int:
	_con_block = false
	_defence += (_arrDice[1]+_con)/3
	emit_signal("change_defence",_defence)
	return (_arrDice[1]+_con)/3
func doAttStr()->int:
	_str_block = false
	return (_arrDice[0]+_str)/3
func doDefStr()->int:
	_str_block = false
	_defence += (_arrDice[0]+_str)/3
	emit_signal("change_defence",_defence)
	return (_arrDice[0]+_str)/3


# Called when the node enters the scene tree for the first time.
func _init():
	_dices = DicePool.new([Dice.new(),Dice.new(),Dice.new()])

func _process(delta):
	if(!isDie and isFight):
		_arrDice = _dices.rollDice()
		if(!_str_block):
			_str_speed += (_analize+_arrDice[0])*delta
		if(!_con_block):
			_con_speed += (_memory+_arrDice[1])*delta
		if(!_dex_block):
			_dex_speed += (_perception+_arrDice[2])*delta
		if(_str_speed>_speed):
			_str_speed = 0
			_str_block = true
			emit_signal("ready_str",_str)
		if(_con_speed>_speed):
			_con_speed = 0
			_con_block = true
			emit_signal("ready_con",_con)
		if(_dex_speed>_speed):
			_dex_speed = 0
			_dex_block = true
			emit_signal("ready_dex",_dex)
		if(_str<=0 or _con<=0 or _dex<=0):
			emit_signal("die")

