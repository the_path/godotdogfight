extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var isFighting:bool = false
# Called when the node enters the scene tree for the first time.
func _ready():
	$Arena/infoLabel.hide()
	randomize()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_TextureButton_pressed():
	if(!isFighting):
		$Arena/infoLabel.hide()
		$Arena/Fighter1.reinit()
		$Arena/Fighter2.reinit()
		$Arena/Fighter1.isFight=true
		$Arena/Fighter2.isFight=true
		isFighting = true
		$Arena/TextureButton/Label.text = 'Fighting'
		$Arena/Fighter1/dex_att.hide()
		$Arena/Fighter1/dex_def.hide()
		$Arena/Fighter1/con_att.hide()
		$Arena/Fighter1/con_def.hide()
		$Arena/Fighter1/str_att.hide()
		$Arena/Fighter1/str_def.hide()
		$Arena/Fighter2/str_att2.hide()
		$Arena/Fighter2/str_def2.hide()
		$Arena/Fighter2/con_att2.hide()
		$Arena/Fighter2/con_def2.hide()
		$Arena/Fighter2/dex_att2.hide()
		$Arena/Fighter2/dex_def2.hide()


func _on_Fighter1_change_con(_con):
	$Arena/Fighter1/con/Label.text = String(_con)


func _on_Fighter1_change_dex(v):
	$Arena/Fighter1/dex/Label.text = String(v)


func _on_Fighter1_change_str(v):
	$Arena/Fighter1/str/Label.text = String(v)


func _on_Fighter1_die():
	isFighting = false # Replace with function body.
	$Arena/infoLabel.show() 
	$Arena/infoLabel.text = 'Fighter 2\n WIN!!!'
	$Arena/Fighter1.isFight=false
	$Arena/Fighter2.isFight=false
	$Arena/Fighter1/dex_att.hide()
	$Arena/Fighter1/dex_def.hide()
	$Arena/Fighter1/con_att.hide()
	$Arena/Fighter1/con_def.hide()
	$Arena/Fighter1/str_att.hide()
	$Arena/Fighter1/str_def.hide()
	$Arena/TextureButton/Label.text = 'START'

func _on_Fighter1_ready_con(v):
	$Arena/Fighter1/con_att.show() 
	$Arena/Fighter1/con_def.show()
	if($Arena/TimerAI_Con.is_stopped()):
		$Arena/TimerAI_Con.start()


func _on_Fighter1_ready_str(v):
	$Arena/Fighter1/str_att.show() 
	$Arena/Fighter1/str_def.show()
	if($Arena/TimerAI_Str.is_stopped()):
		$Arena/TimerAI_Str.start() 


func _on_Fighter1_ready_dex(v):
	$Arena/Fighter1/dex_att.show() 
	$Arena/Fighter1/dex_def.show() 
	if($Arena/TimerAI_Dex.is_stopped()):
		$Arena/TimerAI_Dex.start() 

func _on_dex_def_choose_pressed():
	$Arena/Fighter1.doDefDex()
	$Arena/Fighter1/dex_att.hide()
	$Arena/Fighter1/dex_def.hide()


func _on_dex_att_choose_pressed():
	$Arena/Fighter2.doDMG(0,0,$Arena/Fighter1.doAttDex())
	$Arena/Fighter1/dex_att.hide()
	$Arena/Fighter1/dex_def.hide()


func _on_con_def_choose_pressed():
	$Arena/Fighter1.doDefCon()
	$Arena/Fighter1/con_att.hide()
	$Arena/Fighter1/con_def.hide()


func _on_con_att_choose_pressed():
	$Arena/Fighter2.doDMG(0,$Arena/Fighter1.doAttCon(),0)
	$Arena/Fighter1/con_att.hide()
	$Arena/Fighter1/con_def.hide()


func _on_str_def_choose_pressed():
	$Arena/Fighter1.doDefStr()
	$Arena/Fighter1/str_att.hide()
	$Arena/Fighter1/str_def.hide()


func _on_str_att_choose_pressed():
	$Arena/Fighter2.doDMG($Arena/Fighter1.doAttStr(),0,0)
	$Arena/Fighter1/str_att.hide()
	$Arena/Fighter1/str_def.hide()


func _on_str_att_choose2_pressed():
	$Arena/Fighter1.doDMG($Arena/Fighter2.doAttStr(),0,0)
	$Arena/Fighter2/str_att2.hide()
	$Arena/Fighter2/str_def2.hide()


func _on_str_def_choose2_pressed():
	$Arena/Fighter2.doDefStr()
	$Arena/Fighter2/str_att2.hide()
	$Arena/Fighter2/str_def2.hide()


func _on_con_att_choose2_pressed():
	$Arena/Fighter1.doDMG(0,$Arena/Fighter2.doAttCon(),0)
	$Arena/Fighter2/con_att2.hide()
	$Arena/Fighter2/con_def2.hide()


func _on_con_def_choose2_pressed():
	$Arena/Fighter2.doDefCon()
	$Arena/Fighter2/con_att2.hide()
	$Arena/Fighter2/con_def2.hide()


func _on_dex_att_choose2_pressed():
	$Arena/Fighter1.doDMG(0,0,$Arena/Fighter2.doAttDex())
	$Arena/Fighter2/dex_att2.hide()
	$Arena/Fighter2/dex_def2.hide()


func _on_dex_def_choose2_pressed():
	$Arena/Fighter2.doDefDex()
	$Arena/Fighter2/dex_att2.hide()
	$Arena/Fighter2/dex_def2.hide()


func _on_Fighter2_change_con(v):
	$Arena/Fighter2/con/Label.text = String(v)


func _on_Fighter2_change_dex(v):
	$Arena/Fighter2/dex/Label.text = String(v)


func _on_Fighter2_change_str(v):
	$Arena/Fighter2/str/Label.text = String(v)


func _on_Fighter2_die():
	isFighting = false # Replace with function body.
	$Arena/infoLabel.show() 
	$Arena/infoLabel.text = 'Fighter 1\n WIN!!!'
	$Arena/Fighter1.isFight=false
	$Arena/Fighter2.isFight=false
	$Arena/Fighter2/str_att2.hide()
	$Arena/Fighter2/str_def2.hide()
	$Arena/Fighter2/con_att2.hide()
	$Arena/Fighter2/con_def2.hide()
	$Arena/Fighter2/dex_att2.hide()
	$Arena/Fighter2/dex_def2.hide()
	$Arena/TextureButton/Label.text = 'START'

func _on_Fighter2_ready_con(v):
	
	$Arena/Fighter2/con_att2.show() 
	$Arena/Fighter2/con_def2.show() 


func _on_Fighter2_ready_dex(v):
	$Arena/Fighter2/dex_att2.show() 
	$Arena/Fighter2/dex_def2.show() 


func _on_Fighter2_ready_str(v):
	$Arena/Fighter2/str_att2.show() 
	$Arena/Fighter2/str_def2.show() 


func _on_TimerAI_Str_timeout():
	$Arena/TimerAI_Str.stop()
	var tmp = randi()%2;
	if(tmp ==0):
		_on_str_att_choose_pressed()
	else:
		_on_str_def_choose_pressed()


func _on_TimerAI_Con_timeout():
	$Arena/TimerAI_Con.stop()
	var tmp = randi()%2;
	if(tmp ==0):
		_on_con_att_choose_pressed()
	else:
		_on_con_def_choose_pressed()


func _on_TimerAI_Dex_timeout():
	$Arena/TimerAI_Dex.stop()
	var tmp = randi()%2;
	if(tmp ==0):
		_on_dex_att_choose_pressed()
	else:
		_on_dex_def_choose_pressed()


func _on_Fighter1_change_defence(v):
	$Arena/Fighter1/defence/Label.text = String(v)


func _on_Fighter2_change_defence(v):
	$Arena/Fighter2/defence/Label.text = String(v)
